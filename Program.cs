﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xddddddddddddd
{
    class Program
    {
        static void showMenuTable()
        {
            Console.WriteLine("\nWybierz działanie:");
            Console.WriteLine("[1]Dodaj element");
            Console.WriteLine("[2]Usun element");
            Console.WriteLine("[3]Dodaj element w wolne miejsce");
            Console.WriteLine("[4]Usun wybrana wartosc");
            Console.WriteLine("[5]Pokaz tablice");
            Console.WriteLine("[6]Sortowanie i szukanie binarne");
            Console.WriteLine("[0]Wyjdz");
        }
        static void showSortBubble()
        {
            Console.WriteLine("\nWybierz działanie:");
            Console.WriteLine("[1]Dodaj element");
            Console.WriteLine("[2]Usun element");
            Console.WriteLine("[3]Dodaj element w wolne miejsce");
            Console.WriteLine("[4]Usun wybrana wartosc");
            Console.WriteLine("[5]Pokaz tablice");
            Console.WriteLine("[6]Sortowanie bąbelkowe");
            Console.WriteLine("[0]Wyjdz");
        }
        static void showQuickSort()
        {
            Console.WriteLine("\nWybierz działanie:");
            Console.WriteLine("[1]Dodaj element");
            Console.WriteLine("[2]Usun element");
            Console.WriteLine("[3]Dodaj element w wolne miejsce");
            Console.WriteLine("[4]Usun wybrana wartosc");
            Console.WriteLine("[5]Pokaz tablice");
            Console.WriteLine("[6]Sortowanie metodą \"QuickSort\"");
            Console.WriteLine("[0]Wyjdz");
        }
        static void showMenuStos()
        {
            Console.WriteLine("\nWybierz działanie:");
            Console.WriteLine("[1]Dodaj na stos");
            Console.WriteLine("[2]Usun z stosu");
            Console.WriteLine("[0]Wyjdz");
        }
        static void Main(string[] args)
        {
            string wybor;
            int tableSize;
            int randomSize;
            bool state = true;
            Console.WriteLine("[1]Stos");
            Console.WriteLine("[2]Tablica");
            Console.WriteLine("[3]Sortowanie Bąbelkowe");
            Console.WriteLine("[4]Sortowanie metodą \"QuickSort\"");
            Console.WriteLine("[0]Wyjdz");
            Console.Write("Wybor: ");
            wybor = Console.ReadLine();
            switch (wybor)
            {
                case "0":
                    return;
                case "1":
                    Console.Write("Podaj wielkosc stosu (wieksze od 0): ");
                    while (!int.TryParse(Console.ReadLine(), out tableSize) || !(tableSize > 0))
                        Console.Write("Podaj ilość elementów (większe od 0): ");
                    Stos s = new Stos(tableSize);
                    showMenuStos();
                    while (state)
                    {
                        double tempW;
                        string tempkey = Console.ReadLine();
                        switch (tempkey)
                        {
                            case "0":
                                state = false;
                                break;
                            case "1":
                                Console.Write("Podaj wartosc: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprwaną wartość: ");
                                s.Push(tempW);
                                Console.WriteLine("Dodano do stosu [{0}]", tempW);
                                break;
                            case "2":
                                tempW = s.Pop();
                                Console.WriteLine("Zabrano z stosu [{0}]", tempW);
                                break;
                            default:
                                showMenuStos();
                                break;
                        }
                    }
                    break;
                case "2":
                    Console.Write("Podaj ilość elementów: ");
                    while (!int.TryParse(Console.ReadLine(), out tableSize) || !(tableSize > 0))
                        Console.Write("Podaj ilość elementów (większe od 0): ");

                    Console.Write("Wprowadz ile elementow ma byc losowych (0 lub brak nie uzupelnia): ");
                    string temp = Console.ReadLine();

                    int.TryParse(temp, out randomSize);
                    Tablica t;
                    if (temp.ToLower() == "brak" || randomSize == 0)
                        t = new Tablica(tableSize);
                    else
                        t = new Tablica(tableSize, randomSize);

                    showMenuTable();
                    while (state)
                    {
                        int pos;
                        double tempW;
                        string tempkey = Console.ReadLine();
                        switch (tempkey)
                        {
                            case "1":
                                Console.Write("Podaj pozycje (od 0): ");
                                while (!int.TryParse(Console.ReadLine(), out pos) || !(pos >= 0))
                                    Console.Write("Podaj pozycje (od 0): ");
                                Console.Write("Podaj wartosc: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprwaną wartość: ");
                                t.Set(pos, tempW);
                                Console.WriteLine("Dodano do pozycji [{0}] wartosc [{1}]", pos, tempW);
                                break;
                            case "2":
                                Console.Write("Podaj pozycje usuniecia (od 0): ");
                                while (!int.TryParse(Console.ReadLine(), out pos) || !(pos > 0))
                                    Console.Write("Podaj pozycje usuniecia (od 0): ");
                                t.Remove(pos);
                                Console.WriteLine("Usunieto pozycje [{0}]", pos);
                                break;
                            case "3":
                                Console.Write("Podaj wartość: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprawna wartosc: ");
                                t.Add(tempW);
                                break;
                            case "4":
                                Console.Write("Podaj wartość do usuniecia: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprawna wartosc do usuniecia: ");
                                t.Delete(tempW);
                                break;
                            case "5":
                                for (int i = 0; i < tableSize; i++)
                                {
                                    Console.WriteLine("[{0}] {1}", i, t.Show(i));
                                }
                                break;
                            case "6":
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprwaną wartość: ");
                                int tpos = t.BinarySrc(tempW);
                                if (tpos == -1)
                                    Console.WriteLine("Nie znaleziono");
                                else
                                    Console.WriteLine("Pozycja: " + tpos + " Wartość: " + t.Show(tpos));
                                break;
                            case "0":
                                state = false;
                                break;
                            default:
                                showMenuTable();
                                break;
                        }
                    }
                    break;
                case "3":
                    Console.Write("Podaj ilość elementów: ");
                    while (!int.TryParse(Console.ReadLine(), out tableSize) || !(tableSize > 0))
                        Console.Write("Podaj ilość elementów (większe od 0): ");

                    Console.Write("Wprowadz ile elementow ma byc losowych (0 lub brak nie uzupelnia): ");
                    string tmp = Console.ReadLine();

                    int.TryParse(tmp, out randomSize);
                    SortBubble srt;
                    if (tmp.ToLower() == "brak" || randomSize == 0)
                        srt = new SortBubble(tableSize);
                    else
                        srt = new SortBubble(tableSize, randomSize);

                    showSortBubble();
                    while (state)
                    {
                        int pos;
                        double tempW;
                        string tempkey = Console.ReadLine();
                        switch (tempkey)
                        {
                            case "1":
                                Console.Write("Podaj pozycje (od 0): ");
                                while (!int.TryParse(Console.ReadLine(), out pos) || !(pos >= 0))
                                    Console.Write("Podaj pozycje (od 0): ");
                                Console.Write("Podaj wartosc: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprwaną wartość: ");
                                srt.Set(pos, tempW);
                                Console.WriteLine("Dodano do pozycji [{0}] wartosc [{1}]", pos, tempW);
                                break;
                            case "2":
                                Console.Write("Podaj pozycje usuniecia (od 0): ");
                                while (!int.TryParse(Console.ReadLine(), out pos) || !(pos > 0))
                                    Console.Write("Podaj pozycje usuniecia (od 0): ");
                                srt.Remove(pos);
                                Console.WriteLine("Usunieto pozycje [{0}]", pos);
                                break;
                            case "3":
                                Console.Write("Podaj wartość: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprawna wartosc: ");
                                srt.Add(tempW);
                                break;
                            case "4":
                                Console.Write("Podaj wartość do usuniecia: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprawna wartosc do usuniecia: ");
                                srt.Delete(tempW);
                                break;
                            case "5":
                                for (int i = 0; i < tableSize; i++)
                                {
                                    Console.WriteLine("[{0}] {1}", i, srt.Show(i));
                                }
                                break;
                            case "6":
                                srt.Sort();
                                Console.WriteLine("Posortowoano");
                                break;
                            case "0":
                                state = false;
                                break;
                            default:
                                showSortBubble();
                                break;
                        }
                    }
                    break;
                case "4":
                    Console.Write("Podaj ilość elementów: ");
                    while (!int.TryParse(Console.ReadLine(), out tableSize) || !(tableSize > 0))
                        Console.Write("Podaj ilość elementów (większe od 0): ");

                    Console.Write("Wprowadz ile elementow ma byc losowych (0 lub brak nie uzupelnia): ");
                    string tmpp = Console.ReadLine();

                    int.TryParse(tmpp, out randomSize);
                    QuickSort qsrt;
                    if (tmpp.ToLower() == "brak" || randomSize == 0)
                        qsrt = new QuickSort(tableSize);
                    else
                        qsrt = new QuickSort(tableSize, randomSize);

                    showQuickSort();
                    while (state)
                    {
                        int pos;
                        double tempW;
                        string tempkey = Console.ReadLine();
                        switch (tempkey)
                        {
                            case "1":
                                Console.Write("Podaj pozycje (od 0): ");
                                while (!int.TryParse(Console.ReadLine(), out pos) || !(pos >= 0))
                                    Console.Write("Podaj pozycje (od 0): ");
                                Console.Write("Podaj wartosc: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprwaną wartość: ");
                                qsrt.Set(pos, tempW);
                                Console.WriteLine("Dodano do pozycji [{0}] wartosc [{1}]", pos, tempW);
                                break;
                            case "2":
                                Console.Write("Podaj pozycje usuniecia (od 0): ");
                                while (!int.TryParse(Console.ReadLine(), out pos) || !(pos > 0))
                                    Console.Write("Podaj pozycje usuniecia (od 0): ");
                                qsrt.Remove(pos);
                                Console.WriteLine("Usunieto pozycje [{0}]", pos);
                                break;
                            case "3":
                                Console.Write("Podaj wartość: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprawna wartosc: ");
                                qsrt.Add(tempW);
                                break;
                            case "4":
                                Console.Write("Podaj wartość do usuniecia: ");
                                while (!double.TryParse(Console.ReadLine(), out tempW))
                                    Console.Write("Podaj poprawna wartosc do usuniecia: ");
                                qsrt.Delete(tempW);
                                break;
                            case "5":
                                for (int i = 0; i < tableSize; i++)
                                {
                                    Console.WriteLine("[{0}] {1}", i, qsrt.Show(i));
                                }
                                break;
                            case "6":
                                qsrt.Sort();
                                Console.WriteLine("Posortowoano");
                                break;
                            case "0":
                                state = false;
                                break;
                            default:
                                showQuickSort();
                                break;
                        }
                    }
                    break;
                case "5":
                    while (state)
                    {
                        int texd;
                        Console.Write("Podaj silnie  do obliczenia: ");
                        while (!int.TryParse(Console.ReadLine(), out texd))
                            Console.Write("Podaj poprwaną wartość: ");
                        Factorial xd = new Factorial(texd);
                        Console.WriteLine(xd.Show());
                        Console.ReadKey();
                    }
                    break;
            }
        }
    }
}
