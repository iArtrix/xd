﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xddddddddddddd
{
    public class Tablica
    {
        public static double[] table;
        public int lastElement;
        public Tablica(int size, int random)
        {
            // Funkcja random, do wpisywania liczb losowych
            Random gen = new Random();
            table = new double[size];
            if (size < random)
                random = size;
            for (int i = 0; i < random; i++)
            {
                table[i] = gen.NextDouble();
            }
        }
        public Tablica(int size)
        {
            table = new double[size];
        }
        public void Set(int pos, double val)
        {
            // Ustawianie wartośći, wraz z obsługą błędów
            try
            {
                table[pos] = val;
            }
            catch
            {
                table[table.Length - 1] = val;
                Console.WriteLine("Ustawiono jako ostatni element[{0}]", table.Length - 1);
            }
        }
        public void Remove(int pos)
        {
            // Usuwanie wartości, wraz z obsługą błędów
            try
            {
                table[pos] = 0;
            }
            catch
            {
                table[table.Length - 1] = 0;
                Console.WriteLine("Wyzerowano ostatni element: [{0}]", table.Length - 1);
            }
        }
        public double Show(int pos)
        {
            // Pokazywanie wartości
            try
            {
                return table[pos];
            }
            catch
            {
                Console.WriteLine("Pokazano ostatni element[{0}]", table.Length - 1);
                return table[table.Length - 1];
            }
        }
        public void Add(double ele)
        {
            // Dodawanie, do wolnego miejsca czyli zamienia pierwsze 0
            int i = 0;
            foreach (double e in table)
            {
                if (e == 0)
                {
                    lastElement = i;
                    table[i] = ele;
                }
                i++;
            }
        }
        public void Delete(double ele)
        {
            // Przeszukiwanie tabeli i usuwanie wyszukanej liczby
            int i = 0;
            foreach (double e in table)
            {
                if (e == ele)
                {
                    table[i] = 0.0;
                }
                i++;
            }
        }
        private void Swap(int n, int n1)
        {
            double tmp = table[n];
            table[n] = table[n1];
            table[n1] = tmp;
        }
        public int BinarySrc(double ele)
        {
            int left = 1;
            int right = table.Length - 1;
            Array.Sort(table);
            while (left < right)
            {
                int center = (left + right) / 2;
                if (table[center] < ele)
                {
                    left = center + 1;
                }
                else
                {
                    right = center;
                }
            }
            if (table[left] == ele)
                return left;
            else
                return -1;
        }
    }
}
