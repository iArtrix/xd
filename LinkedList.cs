﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xddddddddddddd
{
    class LinkedList
    {
        private Node head;
        private int size;
        public LinkedList()
        {
            this.head = null;
            this.size = 0;
        }
        public bool Empty
        {
            get { return this.size == 0; }
        }
        public int Count
        {
            get { return this.size; }
        }
        public object Add(int index, object o)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Zbyt niski index: " + index);
            if (index > size)
                index = size;
            Node current = this.head;
            if (this.Empty || index == 0)
            {
                this.head = new Node(o, this.head);
            }else
            {
                for (int i = 0; i < index - 1; i++)
                    current = current.Nastepny;
                current.Nastepny = new Node(o, current.Nastepny);
            }
            size++;
            return o; 
        }
        public object Add(object o)
        {
            return this.Add(size, o);
        }
        public object Remove(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Zbyt niski index: " + index);
            if (this.Empty)
                return null;
            if (index > this.size)
                index = size - 1;
            Node current = this.head;
            object result = null;
            if (index == 0)
            {
                result = current.Data;
                this.head = current.Nastepny;
            }
            else
            {
                for (int i = 0; i < index - 1; i++)
                    current = current.Nastepny;
                current.Nastepny = current.Nastepny.Nastepny;
            }
            size--;
            return result;
        }
        public object this[int index]
        {
            get { return this.Get(index); }
        }
        public void Clear()
        {
            this.head = null;
        }
        public int IndexOf(object o)
        {
            Node current = this.head;

            for(int i = 0; i < this.size; i++)
            {
                if (current.Data.Equals(o))
                    return i;
                current = current.Nastepny;
            }
            return -1;
        }
        public bool Contains(object o)
        {
            return this.IndexOf(o) >= 0;
        }
        public object Get(int index)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("Zbyt niski index: " + index);
            if (this.Empty)
                return null;
            if (index >= this.size)
                index = this.size - 1;
            Node current = this.head;
            for (int i = 0; i < index; i++)
                current = current.Nastepny;

            return current.Data;
        }
    }
}
