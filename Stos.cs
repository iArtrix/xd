﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xddddddddddddd
{
    public class Stos
    {
        public static double[] table;
        // Pozycja stosu
        public int pos = 0;
        public Stos(int size)
        {
            table = new double[size];
        }
        public void Push(double val)
        {
            // Dodanie, do wolnego miejsca element
            try
            {
                table[pos++] = val;
            }
            catch
            {
                table[table.Length - 1] = val;
            }
        }
        public double Pop()
        {
            // Przestawienie pozycji, bez usuwania elementów, działa na zasadzie nadpisywania
            try
            {
                return table[--pos];
            }
            catch
            {
                pos = 0;
                return table[0];
            }
        }
    }
}
