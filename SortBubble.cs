﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xddddddddddddd
{
    class SortBubble : Tablica
    {
        public SortBubble(int size)
            : base(size) { }
        public SortBubble(int size, int random)
            : base(size, random) { }
        private void Swap(int n, int n1)
        {
            double tmp = table[n];
            table[n] = table[n1];
            table[n1] = tmp;
        }
        public void Sort()
        {
            int n = table.Length;
            while (n > 1)
            {
                for (int i = 0; i < n - 1; i++)
                {
                    if (table[i] > table[i + 1])
                    {
                        this.Swap(i, i + 1);
                    }
                }
                --n;
            }
        }
    }
}
