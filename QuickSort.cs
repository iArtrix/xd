﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace xddddddddddddd
{
    class QuickSort : Tablica
    {
        public QuickSort(int size)
            : base(size) { }
        public QuickSort(int size, int random)
            : base(size, random) { }
        private void Swap(int n, int n1)
        {
            double tmp = table[n];
            table[n] = table[n1];
            table[n1] = tmp;
        }
        public void Sort()
        {
            int lo = 0;
            int hi = table.Length - 1;
            Sort(lo, hi);
        }
        public void Sort(int lo, int hi)
        {
            if (lo < hi)
            {
                int p = this.Partition(lo, hi);
                Sort(lo, p - 1);
                Sort(p + 1, hi);
            }
        }
        public int Partition(int lo, int hi)
        {
            double p = table[hi];
            int i = lo - 1;
            for(int x = lo; x < hi; x++)
            {
                if (table[x] < p)
                {
                    i++;
                    this.Swap(i, x);
                }
            }
            this.Swap(i + 1, hi);
            return i + 1;
        }
    }
}
